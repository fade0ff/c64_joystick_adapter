An USB adapter to connect C64/Amiga joysticks to a PC.

See http://www.lemmini.de/C64%20USB%20Adapter/C64_Joystick_Adapter.html

---------------------------------------------------------------
Copyright 2020 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.