/** USB Joystick
	A flexible USB HID-Joystick implementation

	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#include "global.h"
#include <string.h>
#include <stdlib.h>
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "sct.h"
#include "crc.h"
#include "nvm.h"
#include "systime.h"
#include "systick.h"
#include "systime.h"
#include "romapi.h"
#include "cmsis/app_usbd_cfg.h"
#include "cmsis/hid_generic.h"
#include "segger/SEGGER_RTT.h"


//#define RTT_LOG 1
#define RTT_BT  0

#define PIN_DEBOUNCE_COUNT 10
#define NUMBER_OF_AXES     2
#define NUMBER_OF_BUTTONS  8

static USBD_HANDLE_T g_hUsb;
const  USBD_API_T *g_pUsbApi;


/// general channel configuration options
#define CNF_INVERT   (1UL<<7)
#define CNF_DISABLE  0
#define CNF_ENABLE   (1UL<<0)

/// Joystick mode channel configuration options
#define CNF_DIGITAL (1UL<<2)
#define CNF_ANALOG  0

/// Default input pin config
#define PIN_CFG_DEF (PIN_IOCON_NOPULL | PIN_IOCON_HYS | PIN_IOCON_NO_INV | PIN_IOCON_FILTR_OFF | PIN_IOCON_NO_OD | PIN_FILTMODE_BYPASS | PIN_IOCON_CLKDIV1)

////////////////////////////////////////////////////////////////////////////////////////

// index in debounce array
#define BUTTON_1 0
#define BUTTON_2 1
#define BUTTON_3 2
#define BUTTON_4 3
#define BUTTON_5 4
#define BUTTON_6 5
#define BUTTON_7 6
#define BUTTON_8 7
#define UP       8
#define DOWN     9
#define LEFT     10
#define RIGHT    11

////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	u8 resourceL;
	u8 resourceR;
	u8 mode;
} axis_config_t;

typedef struct {
	u8 resource;
	u8 mode;
} button_config_t ;

typedef struct  {
	axis_config_t   axis[NUMBER_OF_AXES];
	button_config_t button[NUMBER_OF_BUTTONS];
	u8 c64_mode;
} config_t;

config_t config = {
	.axis = {
		{ LEFT,      RIGHT,   CNF_ENABLE | CNF_DIGITAL}, //X
		{ UP,        DOWN,    CNF_ENABLE | CNF_DIGITAL}, //Y
	},
	.button = {
		{ BUTTON_1,          CNF_ENABLE },
		{ BUTTON_2,          CNF_ENABLE },
		{ BUTTON_3,          CNF_ENABLE },
		{ BUTTON_4,          CNF_ENABLE },
		{ BUTTON_5,          CNF_ENABLE },
		{ BUTTON_6,          CNF_ENABLE },
		{ BUTTON_7,          CNF_ENABLE },
		{ BUTTON_8,          CNF_ENABLE }
	},
	0
};

u8 hid_output_report[HID_OUT_REPORT_SIZE];


u16 axes_filtered[NUMBER_OF_AXES];

// input debouncing
typedef struct {
	u8 pin;          ///! pin number
	s8 ctr;          ///! debounce counter
	u8 state;        ///! debounce state: 0: debounced, 1: debouncing ongoing
	volatile u8 lvl; ///! debounced logic level
	u8 inact_lvl;    ///! inactive level: 0: low, 1: high
} pin_debounce_t;

/** debounce structure for the input pins */
pin_debounce_t pins[] = {
	/* pin, counter, state, level, inactive level */
	{PIN_BUTTON_1, 0,0,0,1},
	{PIN_BUTTON_2, 0,0,0,1},
	{PIN_BUTTON_3, 0,0,0,1},
	{PIN_BUTTON_4, 0,0,0,1},
	{PIN_BUTTON_5, 0,0,0,1},
	{PIN_BUTTON_6, 0,0,0,1},
	{PIN_BUTTON_7, 0,0,0,1},
	{PIN_BUTTON_8, 0,0,0,1},
	{PIN_UP,       0,0,0,1},
	{PIN_DOWN,     0,0,0,1},
	{PIN_LEFT,     0,0,0,1},
	{PIN_RIGHT,    0,0,0,1}
};

u32 pin_mask;
u8 button5_available;
u32 pin_isp_ctr;

char str_buffer[64];
char str_buffer_bgnd[64];

volatile u32 ms_counter;
volatile u32 counter_10ms;
volatile u16 frame_nr_old;
volatile u8 usb_connected;


/** UART receive buffer */
char uart_rx_buf[64];
/** UART number of received bytes */
volatile u16 uart_rx_len;

char rtt_rx_buf[64];

u32 random() {
	return SCT_GetCounter(LPC_SCT0) ^ PRC_ReverseByteOrder(SCT_GetCounter(LPC_SCT1));
}

u8 pin_level_raw(u8 ch) {
	pin_debounce_t *p = &pins[ch];
	return PIN_ChGet(p->pin) ^ p->inact_lvl;
}

u8 pin_debounce(u8 ch) {
	u8 changed = 0;
	pin_debounce_t *p = &pins[ch];
	u8 lvl = PIN_ChGet(p->pin) ^ p->inact_lvl;
	if (p->state == 0) {
		// not yet debouncing
		if (lvl != p->lvl) {
			// state change -> start debouncing
			p->ctr = PIN_DEBOUNCE_COUNT;
			p->state = 1;
		}
	} else {
		// already debouncing
		if (lvl != p->lvl) {
			// current level is different than the debounced level
			if (--p->ctr <= 0) {
				// new level was debounced
				p->state = 0;
				p->lvl = lvl;
				changed = 1;
			}
		} else {
			// current level is the same as the debounced level
			if (++p->ctr >= PIN_DEBOUNCE_COUNT) {
				// reset debounce state
				p->state = 0;
			}
		}
	}
	return changed;
}

u8 pin_state(u8 ch) {
	return pins[ch].lvl;
}

u16 calc_crc(u8 *data, u16 len) {
	int i;
	CRC_SetMode(CRC_MODE_POLY_CCITT);
	CRC_SetSeed(0xFFFF);
	for (i=0; i<len; i++)
		CRC_SetData8(data[i]);
	return CRC_GetSum();
}

void write_config(void) {
	u8 buffer[sizeof(config)+2];
	u16 crc = calc_crc((u8*)&config, sizeof(config));
	memcpy(&buffer[0],(u8*)&config, sizeof(config));
	buffer[sizeof(config)] = (u8)(crc & 0xff);
	buffer[sizeof(config)+1] = (u8)(crc >> 8);
	(void)NVM_Write(0, &buffer[0], sizeof(config)+2);
}

void read_config(void) {
	u8 buffer[sizeof(config)+2];
	if (NVM_Read(0, buffer, sizeof(config)+2) != 0) {
		// check crc
		u16 crc = calc_crc((u8*)&buffer, sizeof(config));
		u16 crc_nvm = buffer[sizeof(config)] | (buffer[sizeof(config)+1]<<8);
		if (crc == crc_nvm) {
			memcpy((u8*)&config, &buffer[0], sizeof(config));
		}
	}
}

void set_c64_mode(void) {
	if (config.c64_mode != 0) {
		// in C64 mode, invert Button2 and disable the 3.3V pullup
		PIN_ChSetConfig(PIN_BUTTON_2, PIN_CFG_DEF | PIN_IOCON_INV | PIN_IOCON_PULLDOWN);
	} else {
		// in C64 mode, don't invert Button2 and enable the 3.3V pullup
		PIN_ChSetConfig(PIN_BUTTON_2, PIN_CFG_DEF | PIN_IOCON_PULLUP);
	}
	PIN_ChSet(PIN_LED1,   !config.c64_mode);  // low active - switch LED2 on in C64 mode
}

/** Convert integer to string.
	@param z integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2str( int z, char* buffer ) {
	int i = 0;
	int j;
	char tmp;
	unsigned u;

	if( z < 0 ) {
		buffer[0] = '-';
		buffer++;
		u = -z;
	} else
		u = (unsigned)z;
	do {
		buffer[i++] = '0' + u % 10;
		u /= 10;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

/** Convert integer to string.
	@param u unsigned integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2hex( u32 u, char* buffer ) {
	static const char hexchar[] = "0123456789abcdef";
	int i = 0;
	int j;
	char tmp;

	do {
		buffer[i++] = hexchar[u % 16];
		u /= 16;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

char* timestring(u32 t, char *b) {
	int h,m,s,pos;
	h   = t / 3600;
	m = (t / 60) % 60;
	s = t % 60;
	// hour
	if (h<10) {
		b[0] = '0';
		pos = 1;
	} else pos = 0;
	int2str(h,&b[pos]);
	pos = strlen(b);
	b[pos++]= ':';
	// minute
	if (m<10) {
		b[pos++] = '0';
	}
	int2str(m,&b[pos]);
	pos = strlen(b);
	b[pos++]= '.';
	// second
	if (s<10) {
		b[pos++] = '0';
	}
	int2str(s,&b[pos]);
	return b;
}

/** Append space to the end of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_right(char* s, int l) {
	int len = strlen(s);
	while (len<l) {
		s[len]=' ';
		s[len+1]=0;
		len = strlen(s);
	}
}

/** Append space to the start of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_left(char* s, int l) {
	int i;
	int len = strlen(s);

	if (l > len) {
		// shift string right
		for (i=0; i<=len; i++) {
			s[l-i] = s[len-i];
		}
		// fill start with spaces
		for (i=0; i<l-len; i++)
			s[i] = ' ';
	}
}

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;
	int i;
	u32 mask=1;
	for (i=sizeof(pins)/sizeof(pins[0])-1; i>=0; i--) {
		pin_debounce(i);
		mask <<= 1;
		mask |= pin_state(i);
	}
	pin_mask = mask;
}

/*__INLINE*/ u8 convert_axis_voltage(u16 value) {
	return (value>>4)-128;
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	int i;
	u8 cnf;
	s32 temp;
	u8 buttons;

	for (i=0; i<NUMBER_OF_AXES;i++) {
		cnf = config.axis[i].mode;
		if (cnf & CNF_ENABLE) {
			if ( cnf & CNF_DIGITAL )
				temp = 127 + pin_state(config.axis[i].resourceL)*-127 + pin_state(config.axis[i].resourceR)*127;
			else
				temp = convert_axis_voltage(axes_filtered[i]);
			if (cnf & CNF_INVERT)
				temp = 255-temp;
		} else
			temp = 0;
		hid_data[i] = (u8)temp;
	}
	buttons = 0;
	for (i=NUMBER_OF_BUTTONS-1; i>=0 ;i--) {
		cnf = config.button[i].mode;
		buttons <<= 1;
		if (cnf & CNF_ENABLE) {
			buttons |= pin_state(config.button[i].resource);
		}
	}
	hid_data[2] = buttons;

	if (PIN_ChGet(PIN_ISP)==0) {
		pin_isp_ctr++;
		if (pin_isp_ctr == 100) {
			// pressed for 1s -> toggle c64 mode
			config.c64_mode = !config.c64_mode;
			set_c64_mode();
			write_config();
		}
	} else
		pin_isp_ctr=0;

	counter_10ms++;
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
	// check USB
	u16 frame_nr = LPC_USB->INFO & 0x7ff; // FRAME_NR
	// frame counter increasing and VBUSDEBOUNCED
	usb_connected = ( (frame_nr != frame_nr_old) && (LPC_USB->DEVCMDSTAT & (1<<28)));
	frame_nr_old = frame_nr;
}


/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	//int2str(ms_counter/1000, str_buffer);
	//SEGGER_RTT_Write(RTT_LOG, str_buffer, strlen(str_buffer));
	//SEGGER_RTT_Write(RTT_LOG, "\r\n", 2);
	(void)dummy;

}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}

/**
 * @brief	Handle interrupt from USB
 * @return	Nothing
 */
void USB_IRQHandler(void) {
	USBD_API->hw->ISR(g_hUsb);
}

int main(void) {
	u32 pin_config;
	u8  btn_mode;

	SystemInit();
	NVM_Init();

	SEGGER_RTT_Init();

	SYSTIME_Init();                         /* init system timer */
	PIN_Init();

	// Check for Button 2 being pulled hard to ground by C64 Joystick
	//SYSTIME_WaitMs(100);
	//c64_mode = PIN_ChGet(PIN_BUTTON_2)==0;
	read_config();
	set_c64_mode();

	// Check if Buttons 5..8 are connected
	button5_available = PIN_ChGet(PIN_BUTTON_5)!=0;
	if (button5_available != 0) {
		// button 5 is externally pulled up -> deactivate internal pullups
		pin_config = PIN_CFG_DEF | PIN_IOCON_NOPULL;
		btn_mode = CNF_ENABLE;
	} else {
		// not connected - activate pullups
		pin_config = PIN_CFG_DEF | PIN_IOCON_PULLUP;
		btn_mode = CNF_DISABLE;
	}
	PIN_ChSetConfig(PIN_BUTTON_5, pin_config);
	PIN_ChSetConfig(PIN_BUTTON_6, pin_config);
	PIN_ChSet(PIN_LED2,   !button5_available);  // low active - switch LED2 on in C64 mode

	for (int i=4; i<8; i++)
		config.button[i].mode = btn_mode;

	USBD_API_INIT_PARAM_T usb_param;
	USB_CORE_DESCS_T desc;
	ErrorCode_t ret = LPC_OK;
	/* initialize USBD ROM API pointer. */
	g_pUsbApi = (const USBD_API_T *) LPC_ROM_API->pUSBD;

	/* power UP USB Phy */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_USBPHY_PD);
	/* Reset USB block */
	//Chip_SYSCTL_PeriphReset(RESET_USB);

	/* initialize call back structures */
	memset((void *) &usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
	usb_param.usb_reg_base = LPC_USB0_BASE;
	/*	WORKAROUND for artf44835 ROM driver BUG:
	    Code clearing STALL bits in endpoint reset routine corrupts memory area
	    next to the endpoint control data. For example When EP0, EP1_IN, EP1_OUT,
	    EP2_IN are used we need to specify 3 here. But as a workaround for this
	    issue specify 4. So that extra EPs control structure acts as padding buffer
	    to avoid data corruption. Corruption of padding memory doesn’t affect the
	    stack/program behaviour.
	 */
	usb_param.max_num_ep = 2 + 1;
	usb_param.mem_base = USB_STACK_MEM_BASE;
	usb_param.mem_size = USB_STACK_MEM_SIZE;

	/* Set the USB descriptors */
	desc.device_desc = (uint8_t *) USB_DeviceDescriptor;
	desc.string_desc = (uint8_t *) USB_StringDescriptor;

	/* Note, to pass USBCV test full-speed only devices should have both
	 * descriptor arrays point to same location and device_qualifier set
	 * to 0.
	 */
	desc.high_speed_desc = USB_FsConfigDescriptor;
	desc.full_speed_desc = USB_FsConfigDescriptor;
	desc.device_qualifier = 0;


	/* USB Initialization */
	ret = USBD_API->hw->Init(&g_hUsb, &desc, &usb_param);
	if (ret == LPC_OK) {
		ret = usb_hid_init(g_hUsb, (USB_INTERFACE_DESCRIPTOR *) &USB_FsConfigDescriptor[sizeof(USB_CONFIGURATION_DESCRIPTOR)],
			&usb_param.mem_base, &usb_param.mem_size);
		if (ret == LPC_OK) {
			/*  enable USB interrupts */
			NVIC_EnableIRQ(USB0_IRQn);
			/* now connect */
			USBD_API->hw->Connect(g_hUsb, 1);
		}
	}


	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	//HID_SetFeatureReport(&config,sizeof(config_t));

	while (1) {
		/* Handle HID tasks */
		HID_Tasks();
		if (HID_OutputReportReceived()) {
			PRC_Int_Disable();
			HID_GetOutputReport(hid_output_report, sizeof(hid_output_report));
			//SEGGER_RTT_Write(RTT_LOG, (char*)hid_output_report, sizeof(hid_output_report));
			//SEGGER_RTT_Write(RTT_LOG, "\r\n", 2);
			PRC_Int_Enable();
		}

		/* Sleep until next IRQ happens */
		__WFI();
	}
}
