/** MRT library
	Multi Rate Timer
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef MRT_H
#define MRT_H

/* MRT timer interval register INTVAL */

#define MRT_INTVAL_INTERVAL(x)   (((x)-1)&0x7FFFFFFF)
#define MRT_INTVAL_LOAD_NO_FORCE 0
#define MRT_INTVAL_LOAD_FORCE    0x80000000


/** Get interval register for MRT channel
	@param x index of MRT channel [0..3]
	@return INTVAL register
*/
#define MRT_GetIntval(x) (LPC_MRT->Channel[x].INTVAL)

/** Set interval register for MRT channel.
	@param x index of MRT channel [0..3]
	@param y value to set
*/
#define MRT_SetIntval(x,y)  {LPC_MRT->Channel[x].INTVAL = (u32)(y);}


/* MRT timer interval register TIMER (read only) */

/** Get timer register for MRT channel
	@param x index of MRT channel [0..3]
	@return TIMER register
*/
#define MRT_GetTimer(x) (LPC_MRT->Channel[x].TIMER)


/* MRT control register CTRL */

#define MRT_CTRL_INTEN            (1UL << 0)  ///! interrupt enable
#define MRT_CTRL_MODE_INT_REPEAT  0           ///! Repeat interrupt mode
#define MRT_CTRL_MODE_INT_ONCE    (1UL << 1)  ///! One-shot interrupt mode
#define MRT_CTRL_MODE_BUS_STALL   (2UL << 1)  ///! One-shot bus stall mode

/** Get control register for MRT channel
	@param x index of MRT channel [0..3]
	@return CTRL register
*/
#define MRT_GetCtrl(x) (LPC_MRT->Channel[x].CTRL)

/** Set control register for MRT channel.
	@param x index of MRT channel [0..3]
	@param y value to set
*/
#define MRT_SetCtrl(x,y)  {LPC_MRT->Channel[x].CTRL = (u32)(y);}


/* MRT status register STAT */

#define MRT_STAT_INTFLAG (1UL<<0)     ///! Pending interrupt
#define MRT_STAT_RUN     (1UL<<1)     ///! Timer is running.

/** Get status register for MRT channel
	@param x index of MRT channel [0..3]
	@return STAT register
*/
#define MRT_GetStatus(x) (LPC_MRT->Channel[x].STAT)

/** Clear bits in status register for MRT channel.
	@param x index of MRT channel [0..3]
	@param y bitmask with bits to clear
*/
#define MRT_ClrStatus(x,y)  {LPC_MRT->Channel[x].STAT = (u32)(y);}


/* MRT idle channel register IDLE_CH */

/** Get index of first idle MRT channel.
    An idle channel is a channel where RUN and INTFLAG in the status register are 0.
	@return channel index 0..3 or 4 if no channel is idle
*/
#define MRT_GetIdleChannel() ((LPC_MRT->IDLE_CH>>4)&0xf)



/* MRT global interrupt register IRQ_FLAG */

#define MRT_IRQ_FLAG_CH0 (1UL<<0)        ///! Pending interrupt on channel 0
#define MRT_IRQ_FLAG_CH1 (1UL<<1)        ///! Pending interrupt on channel 1
#define MRT_IRQ_FLAG_CH2 (1UL<<2)        ///! Pending interrupt on channel 2
#define MRT_IRQ_FLAG_CH3 (1UL<<3)        ///! Pending interrupt on channel 3

#define MRT_IRQ_FLAG_CH(x) (1UL<<(x))    ///! Pending interrupt on channel x

/** Get global interrupt register for MRT channel
	@return IRQ_FLAG register
*/
#define MRT_GetIntFlags() (LPC_MRT->IRQ_FLAG)

/** Clear bits global interrupt register for MRT channel.
	@param y bitmask with bits to clear
*/
#define MRT_ClrIntFlags(y)  {LPC_MRT->IRQ_FLAG = (u32)(y);}


#endif
