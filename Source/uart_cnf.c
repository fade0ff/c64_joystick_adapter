/** UART library - Configuration
	Communication via asynchronous serial interface (UART)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "uart.h"
#include "dma.h"

/** Automatic selection of DMA mode */
#if UART_USE_DMA
#define UART_FCR_DMAMODE UART_FCR_DMAMODE_SEL  ///! DMA mode
#else
#define UART_FCR_DMAMODE 0                     ///! Interrupt mode
#endif

/** Assign physical UART devices to virtual devices - no need to use them all or in the given order */
const uart_config_t uart_config_ptr[UART_NUM_DEVICES] = {
	/* device 0 */
	{
		(LPC_UART_Type *)LPC_USART0,
		NULL,
		UART_CFG_ENABLE | UART_CFG_DATALEN_8 | UART_CFG_PARITYSEL_NONE | UART_CFG_STOPLEN_1,
	},
	/* device 1 */
	/*{
		LPC_USART1,
		NULL,
		UART_CFG_ENABLE | UART_CFG_DATALEN_8 | UART_CFG_PARITYSEL_NONE | UART_CFG_STOPLEN_1,
	},*/
	/* device 2 */
	/*
	{
		LPC_USART2,
		NULL,
		UART_CFG_ENABLE | UART_CFG_DATALEN_8 | UART_CFG_PARITYSEL_NONE | UART_CFG_STOPLEN_1,
	},
	*/
};
