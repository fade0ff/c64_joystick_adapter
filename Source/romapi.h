/** ROM API
	Call ROM API functions
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef ROMAPI_H
#define ROMAPI_H

/**
 * @brief LPC15XX High level ROM API structure
 */
typedef struct {
	const u32 pUSBD;		///!USBD API function table base address
	const u32 reserved0;	///! Reserved */
	const u32 pCAND;  		//const CAND_API_T *pCAND;			///! C_CAN API function table base address
	const u32 pWRD; 		//const PWRD_API_T *pPWRD;			///! Power API function table base address
	const u32 reserved2;	///! Reserved */
	const u32 pI2CD; 		//I2CD_API_T *pI2CD;				///! I2C driver API function table base address
	const u32 *pDMAD; 		//DMAD_API_T *pDMAD;				///! DMA driver API function table base address
	const u32 *pSPID; 		//SPID_API_T *pSPID;				///! I2C driver API function table base address
	const u32 *pADCD; 		//ADCD_API_T *pADCD;				///! ADC driver API function table base address
	const u32 *pUARTD; 		//UARTD_API_T *pUARTD;				///! UART driver API function table base address
} LPC_ROM_API_T;

/* Pointer to ROM API function address */
#define LPC_ROM_API_BASE_LOC    0x03000200UL
#define LPC_ROM_API     (*(LPC_ROM_API_T * *) LPC_ROM_API_BASE_LOC)

#endif
