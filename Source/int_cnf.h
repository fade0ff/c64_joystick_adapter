/** INT library - Configuration
	Configuration of interrupt levels
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef INT_CNF_H
#define INT_CNF_H

/** define all interrupt levels here (0 is the highest, 31 the lowest priority) */


#define DMA_IRQ_PRIO            0 ///! Handler for DMA interrupts
#define I2C_IRQ_PRIO            1 ///! I2C interrupt priority
#define SPI_IRQ_PRIO            2 ///! Handler for SPI
#define UART_IRQ_PRIO           3 ///! UART receive interrupt
#define RIT_IRQ_PRIO            4 ///! Handler for RIT event
#define DCF77_IRQ_PRIO          7 ///! Handler for DCF77 edges
#define USB_IRQ_PRIO		    8 ///! USB receive interrupt
#define SYSTICK_IRQ_PRIO        8 ///! Handler for time base creation - same prio as USB to avoid issues
#define WRAP_APP_IRQ_PRIO      30 ///! Wrapper interrupt used to wrap down to low priority

#endif
