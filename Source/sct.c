#include "global.h"
#include "prc.h"
#include "sys.h"
#include "sct.h"
#include "dma.h"

//LPC_SCT_T* lpc_sct_device[4] =  {LPC_SCT0, LPC_SCT1, LPC_SCT2, LPC_SCT3};


/** Start the SCT device
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
  */
 void SCT_Start(LPC_SCT_T *device) {
	 PRC_Locked_And((void*)&SCT_GetControl(device), (u32)(~(SCT_CTRL_HALT /*| SCT_CTRL_HALT_HI*/)));
 }

/** Stop the SCT device
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
 */
 void SCT_Stop(LPC_SCT_T *device)  {
	/* Stop the SCT before configuration */
	PRC_Locked_Or((void*)&SCT_GetControl(device), SCT_CTRL_HALT /*| SCT_CTRL_HALT_HI*/);
	/* Clear the counter */
	PRC_Locked_Or((void*)&SCT_GetControl(device), SCT_CTRL_CLRCTR /*)| SCT_CTRL_CLRCTR_HI*/);
 }

/** Setup a PWM channel
    @param device physical device [LPC_SCT0, LPC_SCT1, LPC_SCT2, LPC_SCT3]
    @param match_ch match register to use (1..15 as 0 is used for period)
    @param out_ch output channel to use
    @param polarity 1: high active, 0: low active
 */
void SCT_PWM_SetupChannel(LPC_SCT_T *device, u8 match_ch, u8 out_ch, u8 polarity) {
	u32 init, res;
	u8 evt_idx = match_ch; /* match channel is also used as event index */

	SCT_SetEventControl(device, evt_idx, SCT_EVENT_MATCHSEL(match_ch)|SCT_EVENT_COMBMODE_MATCH);
	SCT_SetEventState(device, evt_idx, 1);                   /* default state: 0 */
	if (polarity != 0) {
		/* high active */
		SCT_SetOutputSet(device, out_ch,  1U);               /* fixed period event: 1 */
		SCT_SetOutputClr(device, out_ch,  1UL<<evt_idx);     /* duty cycle event */
		init = 0;                                            /* low init level */
		res = SCT_RES_CLEAR;                                 /* clear in case of conflict */
	} else {
		/* low active */
		SCT_SetOutputClr(device, out_ch,  1UL);              /* fixed period event: 1 */
		SCT_SetOutputSet(device, out_ch,  1UL<<evt_idx);     /* duty cycle event */
		init =  1UL << out_ch;                               /* high init level */
		res = SCT_RES_SET;                                   /* set in case of conflict */
	}

	/* Output initially off */
	PRC_Locked_Bitmask((void*)&SCT_GetOutput(device), 1UL<<out_ch, init);
	/* Clear the output in-case of conflict */
	PRC_Locked_Bitmask((void*)&SCT_GetConflictRes(device), SCT_RES_MASK<<out_ch, res<<out_ch);
	/* Set and Clear do not depend on direction */
	PRC_Locked_Bitmask((void*)&SCT_GetOutputDirCtrl(device), SCT_DIRCTRL_SETCLR_MASK<<out_ch, SCT_DIRCTRL_SETCLR_INDEP<<out_ch);
}

/** Set the common PWM frequency (always on match0/event0)
    @param device physical device [LPC_SCT0, LPC_SCT1, LPC_SCT2, LPC_SCT3]
    @param frequency common PWM frequency to set
*/
void SCT_PWM_SetupPeriod(LPC_SCT_T *device, uint32_t freq) {
	SCT_Stop(device);

	/* Match 0 must be used as period register */
	PRC_Locked_Bitmask((void*)&SCT_GetRegmode(device), 1UL, SCT_REGMODE_MATCH(0));
	/* Setup match and reload register */
	SCT_SetMatch(device, 0, 0);
	SCT_SetMatchReload(device, 0, (SYS_FRQ/freq)-1); /* outputs use system clock! */

	SCT_SetEventControl(device, 0, SCT_EVENT_MATCHSEL(0)|SCT_EVENT_COMBMODE_MATCH);
	SCT_SetEventState(device, 0, 1);           /* default state: 0 */

	/* Set SCT Counter to count 32-bits and reset to 0 after reaching MATCH0 */
	SCT_SetConfig(device, SCT_CFG_UNIFY_32bit | SCT_CFG_AUTOLIMIT_LO);
}

/** Setup double edge input capture for pulse measurement
    @param device physical device [LPC_SCT0, LPC_SCT1, LPC_SCT2, LPC_SCT3]
    @param capture_ch capture channel to use [0..14]
    @param in_ch SCT input channel channel to use [0..7]
    @note two consecutive capture channels are used, 1st is given here
*/
void SCT_PWM_SetupDualCapture(LPC_SCT_T *device, u8 capture_ch,  u8 in_ch, u8 polarity) {
	u32 leading_edge, trailing_edge;
	u8  capture_event = capture_ch;

	SCT_Stop(device);

	SCT_SetState(device, 0);

	if (polarity != 0) {
		/* high active */
		leading_edge  = SCT_EVENT_IOCOND_RISE;
		trailing_edge = SCT_EVENT_IOCOND_FALL;
	} else {
		/* low active */
		leading_edge  = SCT_EVENT_IOCOND_FALL;
		trailing_edge = SCT_EVENT_IOCOND_RISE;
	}

	/* Set capture events */
	PRC_Locked_Bitmask((void*)&SCT_GetRegmode(device), 1UL, SCT_REGMODE_CAPTURE(capture_ch)|SCT_REGMODE_CAPTURE(capture_ch+1));
	SCT_SetEventControl(device, capture_event,   SCT_EVENT_IOSEL(in_ch)|SCT_EVENT_COMBMODE_IO|leading_edge|SCT_EVENT_STATELD|SCT_EVENT_STATEV(0));
	SCT_SetEventControl(device, capture_event+1, SCT_EVENT_IOSEL(in_ch)|SCT_EVENT_COMBMODE_IO|trailing_edge|SCT_EVENT_STATELD|SCT_EVENT_STATEV(0));

	SCT_SetEventState(device, capture_event,   1);
	SCT_SetEventState(device, capture_event+1, 1);

	SCT_SetLimit(device, (1UL<<capture_event));

	SCT_SetCaptureControl(device, capture_ch,   1UL<<capture_event);
	SCT_SetCaptureControl(device, capture_ch+1, 1UL<<(capture_event+1));

	SCT_SetConfig(device, SCT_CFG_UNIFY_32bit|SCT_CFG_INSYNC(7));
}


/** Setup a PWM burst transfer to change the DC with each transfer
   @param device physical device [LPC_SCT0, LPC_SCT1, LPC_SCT2, LPC_SCT3]
   @param dc_ch match channel (and event) used for DC [1..15]
   @param dma_ch Any DMA channel  [0..18]
   @param dma_0_1 Use DMARequest 0 or 1 [0..1]
   @param buf buffer containing 32bit DC values in ticks
   @param size number of transfers
   @note Use SCT_PWM_StartDMABurst(dma_ch) to actually start the burst
 */
void SCT_PWM_SetupDMABurst(LPC_SCT_T *device, u8 dc_ch, u8 dma_ch, u8 dma_0_1) {
	u8 imux0, imux1, imux;
	u8 event = dc_ch; // assume fixed relation between event and match register for DC event
	u32 *ptr;

	// determine the correct imux values
	switch ((u32)device) {
		case (u32)LPC_SCT0:
			imux0 = DMA_ITRIG_INMUX_SCT0_DMA0;
			imux1 = DMA_ITRIG_INMUX_SCT0_DMA0;
			break;
		case (u32)LPC_SCT1:
			imux0 = DMA_ITRIG_INMUX_SCT1_DMA0;
			imux1 = DMA_ITRIG_INMUX_SCT1_DMA0;
			break;
		case (u32)LPC_SCT2:
			imux0 = DMA_ITRIG_INMUX_SCT2_DMA0;
			imux1 = DMA_ITRIG_INMUX_SCT2_DMA0;
			break;
		default: // LPC_SCT3
			imux0 = DMA_ITRIG_INMUX_SCT3_DMA0;
			imux1 = DMA_ITRIG_INMUX_SCT3_DMA0;
	}
	// consider DMA0/1 request
	if (dma_0_1 == 0) {
		ptr = (void*)&SCT_GetDMA0ReqCtrl(device);
		imux = imux0;
	} else {
		ptr = (void*)&SCT_GetDMA1ReqCtrl(device);
		imux = imux1;
	}
	// configure DMA input multiplexer
	DMA_SetItrigInmux(dma_ch, imux);
	// set DMA request for event
	PRC_Locked_Bitmask(ptr, SCT_DMAREQ_EVENT_MASK, SCT_DMAREQ_EVENT(event));
	// enable DMA channel
	DMA_EnableCh(dma_ch,1);
	// for some reason this only works when activating the burst transfer
	DMA_SetChConfig(dma_ch, DMA_CFG_HWTRIGEN|DMA_CFG_TRIGBURST_BURST|DMA_CFG_TRIGPOL_HI|DMA_CFG_CHPRIORITY(DMA_PRIO_SCT_BURST));
	DMA_SetChDestAdr(dma_ch, (u32)&SCT_GetMatchReload(device, dc_ch));
	DMA_SetChLinkAdr(dma_ch, 0);
}
/* Start DMA burst.
   @param dma_ch DMA channel used [0..18]
   @param buf buffer with period data
   @param size number of transfers
   @note 32bit periods transferred to unified match reload
*/
void SCT_PWM_StartDMABurst32(u8 dma_ch, u32 *buf, u16 size) {
	DMA_SetChSrcAdr(dma_ch, (u32)buf + (size-1)*4);
	DMA_SetChXferConfig(dma_ch, DMA_XFERCFG_WIDTH_32|DMA_XFERCFG_CLRTRIG|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SRCINC_1|DMA_XFERCFG_XFERCOUNT(size));
}

/* Start DMA burst.
   @param dma_ch DMA channel used [0..18]
   @param buf buffer with period data
   @param size number of transfers
   @note 16bit periods transferred to lower match reload
*/
void SCT_PWM_StartDMABurst16(u8 dma_ch, u16 *buf, u16 size) {
	DMA_SetChSrcAdr(dma_ch, (u32)buf + (size-1)*2);
	DMA_SetChXferConfig(dma_ch, DMA_XFERCFG_WIDTH_16|DMA_XFERCFG_CLRTRIG|DMA_XFERCFG_CFGVALID|DMA_XFERCFG_SRCINC_1|DMA_XFERCFG_XFERCOUNT(size));
}
