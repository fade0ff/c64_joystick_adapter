/** State Configurable Timers Library
	Used for PWMs etc.
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


/* Config register CONFIG */
#define SCT_CFG_UNIFY_16bit             0               ///! SCT operates as two 16bit counters
#define SCT_CFG_UNIFY_32bit             1               ///! SCT operates as one 32bit counters
#define SCT_CFG_CLKMODE_SYSTEM          0               ///! System clock used for SCT
#define SCT_CFG_CLKMODE_PRSC_SYSTEM (1<<1)              ///! Prescaled system clock used for SCT
#define SCT_CFG_CLKMODE_SCT_INPUT   (2<<1)              ///! SCT input
#define SCT_CFG_CLKMODE_PRSC_SCT_IN (3<<1)              ///! Prescaled SCT input
#define SCT_CFG_CLKSEL_RISING(x)    ((((u32)(x)&07)*2)<<3)   ///! Rising edge on input x
#define SCT_CFG_CLKSEL_FALLING(x)   ((((u32)(x)&07)*2+1)<<3) ///! Falling edge on input x
#define SCT_CFG_NORELOAD_LO         (1UL<<7)            ///! Prevent reload of lower/unified match and fraction match registers
#define SCT_CFG_NORELOAD_HI         (1UL<<8)            ///! Prevent reload of higher match and fraction match registers
#define SCT_CFG_INSYNC(x)           (((u32)(x)&7)<<9)        ///! Synchronize input x to SCT clock
#define SCT_CFG_AUTOLIMIT_LO        (1UL<<17)           ///! Treat match on R0 as limit condition (lower or unified registers)
#define SCT_CFG_AUTOLIMIT_HI        (1UL<<18)           ///! Treat match on R0 as limit condition (higher registers)


/** Get SCT config register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return config register CFG
 */
#define SCT_GetConfig(x)       ((x)->CONFIG)

/** Set SCT config register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to config register (construct from SCT_CFG_xxx macros)
 */
#define SCT_SetConfig(x,y)     { (x)->CONFIG = (u32)(y); }

/* Control register CTRL_U/CTRL_L/CTRL_H */
#define SCT_CTRL_DOWN               (1<<0)              ///! Counter counting down
#define SCT_CTRL_STOP               (1<<1)              ///! Stop counter
#define SCT_CTRL_HALT               (1<<2)              ///! Halt counter
#define SCT_CTRL_CLRCTR             (1<<3)              ///! Writing a 1 clears the counter
#define SCT_CTRL_BIDIR              (1<<4)              ///! Bidirectional counter
#define SCT_CTRL_PRE(x)             (((u32)(x)&0xff)<<5)     ///! Select prescaler (1..256)
#define SCT_CTRL_PRE_MASK           (255UL<<5)          ///! Mask for prescaler selection
/* Use the HI values only when writing to the 32bit register */
#define SCT_CTRL_DOWN_HI            (1UL<<16)           ///! Counter counting down
#define SCT_CTRL_STOP_HI            (1UL<<17)           ///! Stop counter
#define SCT_CTRL_HALT_HI            (1UL<<18)           ///! Halt counter
#define SCT_CTRL_CLRCTR_HI          (1UL<<19)           ///! Writing a 1 clears the counter
#define SCT_CTRL_BIDIR_HI           (1UL<<20)           ///! Bidirectional counter
#define SCT_CTRL_PRE_HI(x)          (((u32)(x)&0xff)<<21)    ///! Select prescaler (1..256)

/** Get SCT control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return control register CTRL_U
 */
#define SCT_GetControl(x)       ((x)->CTRL_U)

/** Set SCT control register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to control register (construct from SCT_CTRL_xxx macros)
 */
#define SCT_SetControl(x,y)     { (x)->CTRL_U = (y); }

/** Get lower SCT control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return control register CTRL_L
 */
#define SCT_GetControlLo(x)       ((x)->CTRL_L)

/** Set lower SCT control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to control register (construct from SCT_CTRL_xxx macros)
 */
#define SCT_SetControlLo(x,y)     { (x)->CTRL_L = (u16)(y); }


/** Get higher SCT control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return control register CFGCTRL_H
 */
#define SCT_GetControlHi(x)       ((x)->CTRL_H)

/** Set higher SCT control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to control register (construct from SCT_CTRL_xxx macros)
 */
#define SCT_SetControlHi(x,y)     { (x)->CTRL_H = (u16)(y); }


/* Limit register LIMIT_U/LIMIT_L/LIMIT_H */
/** Get SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return limit register LIMIT
 */
#define SCT_GetLimit(x)       ((x)->LIMIT)

/** Set SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to limit register
 */
#define SCT_SetLimit(x,y)     { (x)->LIMIT = (y); }

/** Get lower SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return limit register LIMIT_L
 */
#define SCT_GetLimitLo(x)       ((x)->LIMIT_L)

/** Set lower SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to limit register
 */
#define SCT_SetLimitLo(x,y)     { (x)->LIMIT_L = (u16)(y); }


/** Get higher SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return limit register LIMIT_H
 */
#define SCT_GetLimitHi(x)       ((x)->LIMIT_H)

/** Set higher SCT limit register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to limit register
 */
#define SCT_SetLimitHi(x,y)     { (x)->LIMIT_H = (u16)(y); }


/* Halt condition register */
/** Get SCT halt condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return halt condition register HALT
 */
#define SCT_GetHalt(x)       ((x)->HALT)

/** Set SCT halt condition register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to halt condition register
 */
#define SCT_SetHalt(x,y)     { (x)->HALT = (y); }

/** Get lower SCT halt condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return halt condition register HALT_L
 */
#define SCT_GetHaltLo(x)       ((x)->HALT_L)

/** Set lower SCT halt condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to halt condition register
 */
#define SCT_SetHaltLo(x,y)     { (x)->HALT_L = (u16)(y); }


/** Get higher SCT halt condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return halt condition register HALT_H
 */
#define SCT_GetHaltHi(x)       ((x)->HALT_H)

/** Set higher SCT halt condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to halt condition register
 */
#define SCT_SetHaltHi(x,y)     { (x)->HALT_H = (u16)(y); }


/* Stop condition register */
/** Get SCT stop condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return stop condition register STOP
 */
#define SCT_GetStop(x)       ((x)->STOP)

/** Set SCT stop condition register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to stop condition register
 */
#define SCT_SetStop(x,y)     { (x)->STOP = (y); }

/** Get lower SCT stop condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return stop condition register STOP_L
 */
#define SCT_GetStopLo(x)       ((x)->STOP_L)

/** Set lower SCT stop condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to stop condition register
 */
#define SCT_SetStopLo(x,y)     { (x)->STOP_L = (u16)(y); }


/** Get higher SCT stop condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return stop condition register STOP_H
 */
#define SCT_GetStopHi(x)       ((x)->STOP_H)

/** Set higher SCT stop condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to stop condition register
 */
#define SCT_SetStopHi(x,y)     { (x)->STOP_H = (u16)(y); }


/* Start condition register */
/** Get SCT start condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return start condition register START
 */
#define SCT_GetStart(x)       ((x)->START)

/** Set SCT start condition register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to start condition register
 */
#define SCT_SetStart(x,y)     { (x)->START = (y); }

/** Get lower SCT start condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return start condition register START_L
 */
#define SCT_GetStartLo(x)       ((x)->START_L)

/** Set lower SCT start condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to start condition register
 */
#define SCT_SetStartLo(x,y)     { (x)->START_L = (u16)(y); }


/** Get higher SCT start condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return start condition register START_H
 */
#define SCT_GetStartHi(x)       ((x)->START_H)

/** Set higher SCT start condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to start condition register
 */
#define SCT_SetStartHi(x,y)     { (x)->START_H = (u16)(y); }


/* Dither condition register */
/** Get SCT dither condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return dither condition register DITHER
 */
#define SCT_GetDither(x)       ((x)->DITHER)

/** Set SCT dither condition register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to dither condition register
 */
#define SCT_SetDither(x,y)     { (x)->DITHER = (y); }

/** Get lower SCT dither condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return dither condition register DITHER_L
 */
#define SCT_GetDitherLo(x)       ((x)->DITHER_L)

/** Set lower SCT dither condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to dither condition register
 */
#define SCT_SetDitherLo(x,y)     { (x)->DITHER_L = (u16)(y); }


/** Get higher SCT dither condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return dither condition register DITHER_H
 */
#define SCT_GetDitherHi(x)       ((x)->DITHER_H)

/** Set higher SCT dither condition register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to dither condition register
 */
#define SCT_SetDitherHi(x,y)     { (x)->DITHER_H = (u16)(y); }


/* Counter register */
/** Get SCT counter register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return counter register CFG
 */
#define SCT_GetCounter(x)       ((x)->COUNT_U)

/** Set SCT counter register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to counter register
 */
#define SCT_SetCounter(x,y)     { (x)->COUNT_U = (y); }

/** Get lower SCT counter register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return counter register COUNT_L
 */
#define SCT_GetCounterLo(x)       ((x)->COUNT_L)

/** Set lower SCT counter register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to counter register
 */
#define SCT_SetCounterLo(x,y)     { (x)->COUNT_L = (u16)(y); }


/** Get higher SCT counter register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return counter register COUNT_H
 */
#define SCT_GetCounterHi(x)       ((x)->COUNT_H)

/** Set higher SCT counter register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to counter register
 */
#define SCT_SetCounterHi(x,y)     { (x)->COUNT_H = (u16)(y); }


/* State register */
/** Get SCT state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return state register STATE
 */
#define SCT_GetState(x)       ((x)->STATE)

/** Set SCT state register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to state register
 */
#define SCT_SetState(x,y)     { (x)->STATE = (y); }

/** Get lower SCT state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return state register STATE_L
 */
#define SCT_GetStateLo(x)       ((x)->STATE_L)

/** Set lower SCT state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to state register
 */
#define SCT_SetStateLo(x,y)     { (x)->STATE_L = (u16)(y); }


/** Get higher SCT state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return state register STATE_H
 */
#define SCT_GetStateHi(x)       ((x)->STATE_H)

/** Set higher SCT state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to state register
 */
#define SCT_SetStateHi(x,y)     { (x)->STATE_H = (u16)(y); }


/* Input register */
#define SCT_INPUT_AIN(x)            (1UL<<(x))          ///! State of input x. Direct read */
#define SCT_INPUT_SIN(x)            (1UL<<(16+(x)))     ///! State of input x. Synchronized read (if INSYNC is set) */

/** Get SCT input register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return input register INPUT
 */
#define SCT_GetInput(x)         ((x)->INPUT)

/** Read input channel (direct read).
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y input number [0..7]
	@return input state [0,1]
 */
#define SCT_GetInputCh(x,y)          ( ((x)->INPUT & SCT_INPUT_AIN(y)) != 0 ? 1: 0)

/** Read input channel (synchronized read).
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y input number [0..7]
	@return input state [0,1]
 */
#define SCT_GetInputChSync(x,y)      ( ((x)->INPUT & SCT_INPUT_SIN(y)) != 0 ? 1: 0)

/* Match/capture registers mode register */

#define SCT_REGMODE_CAPTURE(x)       (1UL<<(x))          ///! Set Capture mode for register x [0..15}
#define SCT_REGMODE_MATCH(x)         0                   ///! Set Match mode for register x [0..15]
/* Use the HI values only when writing to the 32bit register */
#define SCT_REGMODE_CAPTURE_HI(x)    (1UL<<(16+(x)))     ///! Set Capture mode for register x [0..15}
#define SCT_REGMODE_MATCH_HI(x)      0                   ///! Set Match mode for register x [0..15]

/** Get SCT match/capture registers mode register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return match/capture registers mode register REGMODE
 */
#define SCT_GetRegmode(x)       ((x)->REGMODE)

/** Set SCT match/capture registers mode register.
		@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to match/capture registers mode register
 */
#define SCT_SetRegmode(x,y)     { (x)->REGMODE = (y); }

/** Get lower SCT match/capture registers mode register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return match/capture registers mode register REGMODE_L
 */
#define SCT_GetRegmodeLo(x)       ((x)->REGMODE_L)

/** Set lower SCT match/capture registers mode register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to match/capture registers mode register
 */
#define SCT_SetRegmodeLo(x,y)     { (x)->REGMODE_L = (u16)(y); }


/** Get higher SCT match/capture registers mode register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return match/capture registers mode register REGMODE_H
 */
#define SCT_GetRegmodeHi(x)       ((x)->REGMODE_H)

/** Set higher SCT match/capture registers mode register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to write to match/capture registers mode register
 */
#define SCT_SetMRegmodeHi(x,y)     { (x)->REGMODE_H = (u16)(y); }


/* Output register */
#define SCT_OUTPUT(x)                (1UL<<(x))          ///! State of output x

/** Get SCT output register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return output register OUTPUT
 */
#define SCT_GetOutput(x)         ((x)->OUTPUT)

/** Read output channel.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output number [0..9]
	@return output state [0,1]
 */
#define SCT_GetOutputCh(x,y)          ( ((x)->OUTPUT & SCT_OUTPUT(y)) != 0 ? 1: 0)

/** Set SCT output register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return output register OUTPUT
 */
#define SCT_SetOutput(x,y)         {(x)->OUTPUT = (u32)(y); }

/** Set output channel (unprotected read/modify).
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output number [0..9]
 */
#define SCT_SetOutputCh(x,y)          { ((x)->OUTPUT |= SCT_OUTPUT(y));}


/* Bidirectional output control register */

#define SCT_DIRCTRL_SETCLR_INDEP     0                   ///! Set and clear do not depend on any counter
#define SCT_DIRCTRL_SETCLR_REV       1UL                 ///! Set and clear are reversed when counter L or the unified counter is counting down
#define SCT_DIRCTRL_SETCLR_REV_HI    2UL                 ///! Set and clear are reversed when counter H is counting down. Do not use if UNIFY = 1
#define SCT_DIRCTRL_SETCLR_MASK      3UL                 ///! Mask to clear entry

#define SCT_DIRCTRL_SETCLR(x,y)      (((u32)(x)&3)<<(y)) ///! Define set and cleat behavior y for output x [0..9]

/** Get SCT output direction control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return output direction control register OUTPUTDIRCTRL
 */
#define SCT_GetOutputDirCtrl(x)      ((x)->OUTPUTDIRCTRL)

/** Set SCT output direction control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [construct from SCT_DIRCTRL_SETCLR(x,y)]
 */
#define SCT_SetOutputDirCtrl(x,y)    {(x)->OUTPUTDIRCTRL = (u32)(y); }

/** Set SCT output direction control for a channel.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output number [0..9]
	@param z value to set [use SCT_DIRCTRL_SETCLR_xxx]
 */
#define SCT_SetOutputDirCtrlCh(x,y,z)    {(x)->OUTPUTDIRCTRL |= (u32)SCT_DIRCTRL_SETCLR(y,z); }


/* Conflict resolution register */
#define SCT_RES_NO_CHANGE        0                   ///! No change in case of conflict
#define SCT_RES_SET              1UL                 ///! Set output in case of conflict (or clear based on the SETCLR0 field)
#define SCT_RES_CLEAR            2UL                 ///! Clear output in case of conflict (or set based on the SETCLR0 field)
#define SCT_RES_TOGGLE           3UL                 ///! Toggle in case of conflict
#define SCT_RES_MASK             3UL                 ///! Mask for clearing the field

#define SCT_RES_SETRES(x,y)      (((x)&3)<<(y))      ///! Define set and clear behavior y for output x [0..9]

/** Get SCT conflict resolution register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return conflict resolution register RES
 */
#define SCT_GetConflictRes(x)          ((x)->RES)

/** Set SCT conflict resolution register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [construct from SCT_RES_SETCLR(x,y)]
 */
#define SCT_SetConflictRes(x,y)         {(x)->RES = (u32)(y); }

/** Set SCT conflict resolution register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output number [0..9]
	@param z value to set [use SCT_RES_SETCLR_xxx]
 */
#define SCT_SetConflictResCh(x,y,z)    {(x)->RES |= (u32)SCT_RES_SETRES(y,z); }


/* DMA Request control */

#define SCT_DMAREQ_EVENT(x)      (1UL<<(x))          ///! Event x [0..15] sets DMA request
#define SCT_DMAREQ_EVENT_MASK    0xffff              ///! Event mask
#define SCT_DMAREQ_DRL           (1UL<<30)           ///! DMA request on reload
#define SCT_DMAREQ_DRQ           (1UL<<31)           ///! State of DMA request (read only)

/** Get SCT DMA0 request control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return conflict resolution register DMA0REQUEST
 */
#define SCT_GetDMA0ReqCtrl(x)          ((x)->DMA0REQUEST)

/** Set SCT DMA0 request control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_DMAREQ_xxx]
 */
#define SCT_SetDMA0ReqCtrl(x,y)      {(x)->DMA0REQUEST = (u32)(y); }

/** Get SCT DMA1 request control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return conflict resolution register DMA0REQUEST
 */
#define SCT_GetDMA1ReqCtrl(x)          ((x)->DMA1REQUEST)

/** Set SCT DMA1 request control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_DMAREQ_xxx]
 */
#define SCT_SetDMA1ReqCtrl(x,y)      {(x)->DMA1REQUEST = (u32)(y); }


/* Interrupt enable register */
#define SCT_EVENT_ENABLE(x)      (1UL<<(x))          ///! Enable event x [0..15]

/** Get SCT Interrupt enable register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return interrupt enable register EVEN
 */
#define SCT_GetIntEnable(x)          ((x)->EVEN)

/** Set SCT Interrupt enable register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_EVENT_xxx]
 */
#define SCT_SetIntEnable(x,y)      {(x)->EVEN = (u32)(y); }


/* Event flag register */
#define SCT_EVENT_FLAG(x)          (1UL<<(x))          ///! Mask event x [0..15]

/** Get SCT Interrupt request flag register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return interrupt request flag register EVFLAG
 */
#define SCT_GetIntReq(x)          ((x)->EVFLAG)

/** Set SCT Interrupt request flag register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_EVFLAG_xxx]
 */
#define SCT_SetIntReq(x,y)      {(x)->EVFLAG = (u32)(y); }

/** Clear SCT interrupt request flag.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y event number [0..15]
 */
#define SCT_ClrIntReqCh(x,y)    {(x)->EVFLAG |= (u32)SCT_EVENT_FLAG(y); }


/* Conflict enable register */
#define SCT_CONEN_ENABLE(x)      (1UL<<(x))          ///! Output x [0..9]

/** Get SCT Conflict enable register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return conflict enable register CONEN
 */
#define SCT_GetConflictEnable(x)          ((x)->CONEN)

/** Set SCT Conflict enable register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_CONEN_xxx]
 */
#define SCT_SetConflictEnable(x,y)      {(x)->CONEN = (u32)(y); }


/* Conflict flag register */
#define SCT_CONFLAG_FLAG(x)        (1UL<<(x))          ///! Output x [0..9]
#define SCT_CONFLAG_BUSERR         (1UL<<30)           ///! Bus error low/unified
#define SCT_CONFLAG_BUSERR_HI      (1UL<<30)           ///! Bus error high

/** Get SCT conflict flag register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@return conflict flag register CONFLAG
 */
#define SCT_GetConflictFlag(x)          ((x)->CONFLAG)

/** Set SCT Conflict flag register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y value to set [use SCT_EVFLAG_xxx]
 */
#define SCT_SetConflictFlag(x,y)      {(x)->CONFLAG = (u32)(y); }

/** Clear SCT conflict flag.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output x [0..9]
 */
#define SCT_ClrConflictFlagCh(x,y)    {(x)->CONFLAG |= (u32)SCT_CONFLAG_FLAG(y); }


/* Match register */
/** Get SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match register MATCH
 */
#define SCT_GetMatch(x,y)       ((x)->MATCH[y].U)

/** Set SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match register
 */
#define SCT_SetMatch(x,y,z)    { (x)->MATCH[y].U = (u32)(z); }

/** Get lower SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match register MATCH_L
 */
#define SCT_GetMatchLo(x)       ((x)->MATCH[y].L)

/** Set lower SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match register

 */
#define SCT_SetMatchLo(x,y)     { (x)->MATCH[y].L = (u16)(z); }

/** Get higher SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match register MATCH_H
 */
#define SCT_GetMatchHi(x)       ((x)->MATCH[y].H)

/** Set higher SCT match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match register
 */
#define SCT_SetMatchHi(x,y,z)    { (x)->MATCH[y].H = (u16)(z); }

/* Match reload register */
/** Get SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match reload register MATCHREL
 */
#define SCT_GetMatchReload(x,y)       ((x)->MATCHREL[y].U)

/** Set SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match reload register
 */
#define SCT_SetMatchReload(x,y,z)     { (x)->MATCHREL[y].U = (u32)(z); }

/** Get lower SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match reload register MATCHREL_L
 */
#define SCT_GetMatchReloadLo(x)       ((x)->MATCHREL[y].L)

/** Set lower SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match reload register

 */
#define SCT_SetMatchReloadLo(x,y,z)   { (x)->MATCHREL[y].L = (u16)(z); }

/** Get higher SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return match reload register MATCHREL_H
 */
#define SCT_GetMatchReloadHi(x)       ((x)->MATCHREL[y].H)

/** Set higher SCT match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to match reload register
 */
#define SCT_SetMatchReloadHi(x,y,z)   { (x)->MATCHREL[y].H = (u16)(z); }


/* Fractional match register */
/** Get SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@return fractional match register FRACMAT
 */
#define SCT_GetFractMatch(x,y)       ((x)->FRACMAT[y].U)

/** Set SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@param z value to write to fractional match register
 */
#define SCT_SetFractMatch(x,y,z)   { (x)->FRACMAT[y].U = (u32)(z); }

/** Get lower SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@return fractional match register FRACMAT_L
 */
#define SCT_GetFractMatchLo(x)       ((x)->FRACMAT[y].L)

/** Set lower SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@param z value to write to fractional match register

 */
#define SCT_SetFractMatchLo(x,y,z)   { (x)->FRACMAT[y].L = (u16)(z); }

/** Get higher SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@return fractional match register FRACMAT_H
 */
#define SCT_GetFractMatchHi(x)       ((x)->FRACMAT[y].H)

/** Set higher SCT fractional match register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..5]
	@param z value to write to fractional match register
 */
#define SCT_SetFractMatchHi(x,y,z)   { (x)->FRACMAT[y].H = (u16)(z); }


/* Fractional Match reload register */
/** Get SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return fractional match reload register FRACMATREL
 */
#define SCT_GetFractMatchReload(x,y)       ((x)->FRACMATREL[y].U)

/** Set SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to fractional match reload register
 */
#define SCT_SetFractMatchReload(x,y,z)   { (x)->FRACMATREL[y].U = (u32)(z); }

/** Get lower SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return fractional match reload register FRACMATREL_L
 */
#define SCT_GetFractMatchReloadLo(x)       ((x)->FRACMATREL[y].L)

/** Set lower SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to fractional match reload register

 */
#define SCT_SetFractMatchReloadLo(x,y,z)   { (x)->FRACMATREL[y].L = (u16)(z); }

/** Get higher SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return fractional match reload register FRACMATREL_H
 */
#define SCT_GetFractMatchReloadHi(x)       ((x)->FRACMATREL[y].H)

/** Set higher SCT fractional match reload register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to fractional match reload register
 */
#define SCT_SetFractMatchReloadHi(x,y,z)   { (x)->FRACMATREL[y].H = (u16)(z); }


/* Capture register */
/** Get SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture register CAP
 */
#define SCT_GetCapture(x,y)       ((x)->CAP[y].U)

/** Set SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture register
 */
#define SCT_SetCapture(x,y,z)    { (x)->CAP[y].U = (u32)(z); }

/** Get lower SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture register CAP_L
 */
#define SCT_GetCaptureLo(x)       ((x)->CAP[y].L)

/** Set lower SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture register

 */
#define SCT_SetCaptureLo(x,y,z)   { (x)->CAP[y].L = (u16)(z); }

/** Get higher SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture register CAP_H
 */
#define SCT_GetCaptureHi(x)       ((x)->CAP[y].H)

/** Set higher SCT capture register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture register
 */
#define SCT_SetCaptureHi(x,y,z)   { (x)->CAP[y].H = (u16)(z); }


/* Capture control register */
/** Get SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture control register CAPCTRL
 */
#define SCT_GetCaptureControl(x,y)       ((x)->CAPCTRL[y].U)

/** Set SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture control register
 */
#define SCT_SetCaptureControl(x,y,z)     { (x)->CAPCTRL[y].U = (u32)(z); }

/** Get lower SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture control register CAPCTRL_L
 */
#define SCT_GetCaptureControlLo(x)       ((x)->CAPCTRL[y].L)

/** Set lower SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture control register
 */
#define SCT_SetCaptureControlLo(x,y,z)   { (x)->CAPCTRL[y].L = (u16)(z); }

/** Get higher SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@return capture control register CAPCTRL_H
 */
#define SCT_GetCaptureControlHi(x)       ((x)->CAPCTRL[y].H)

/** Set higher SCT capture control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y register number [0..15]
	@param z value to write to capture control register
 */
#define SCT_SetCaptureControlHi(x,y,z)   { (x)->CAPCTRL[y].H = (u16)(z); }


/* Event state register */
#define SCT_EVENT_STATE(x)        (1UL<<(x))          ///! State x [0..15]

/** Get SCT event state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y event number [0..15]
	@return event state register EVENT.STATE
 */
#define SCT_GetEventState(x,y)       ((x)->EVENT[y].STATE)

/** Set SCT event state register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y event number [0..15]
	@param z value to write to event state register
 */
#define SCT_SetEventState(x,y,z)     { (x)->EVENT[y].STATE = (u32)(z); }


/* Event control register */
#define SCT_EVENT_MATCHSEL(x)     ((x)&15)            ///! Match register x [0..15]
#define SCT_EVENT_HI_EVENT        (1UL<<4)            ///! Set for high match, 0 for low/unified
#define SCT_EVENT_OUTSEL          (1UL<<5)            ///! Set for output, 0 for input
#define SCT_EVENT_IOSEL(x)        (((u32)(x)&15)<<6)  ///! Select input [0..15] or output [0..9]
#define SCT_EVENT_IOCOND_LOW      0                   ///! Low condition for event
#define SCT_EVENT_IOCOND_RISE     (1UL<<10)           ///! Rising edge condition for event
#define SCT_EVENT_IOCOND_FALL     (2UL<<10)           ///! Falling edge condition for event
#define SCT_EVENT_IOCOND_HIGH     (3UL<<10)           ///! High condition for event
#define SCT_EVENT_COMBMODE_OR      0                  ///! Event occurs on either match or I/O condition
#define SCT_EVENT_COMBMODE_MATCH  (1UL<<12)           ///! Event occurs on match
#define SCT_EVENT_COMBMODE_IO     (2UL<<12)           ///! Event occurs on I/O condition
#define SCT_EVENT_COMBMODE_AND    (3UL<<12)           ///! Event occurs if both, match and I/O condition occur simultaneously
#define SCT_EVENT_STATELD         (1UL<<14)           ///! 0: STATEV value is added into STATE, 1: STATEV value is loaded into STATE
#define SCT_EVENT_STATEV(x)       (((u32)(x)&31)<<15) ///! Value [0..31] loaded into or added to the state selected by HEVENT
#define SCT_EVENT_MATCHMEM        (1UL<<20)           ///! 0: Match if equal, 1: match on ge/le depending on direction
#define SCT_EVENT_DIRECTION_NONE  0                   ///! Event trigger is direction independent
#define SCT_EVENT_DIRECTION_UP    (1UL<<21)           ///! Event trigger only when counting up (in BIDIR mode)
#define SCT_EVENT_DIRECTION_DOWN  (2UL<<21)           ///! Event trigger only when counting down (in BIDIR mode)

/** Get SCT event control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y event number [0..15]
	@return event control register EVENT.CTRL
 */
#define SCT_GetEventControl(x,y)       ((x)->EVENT[y].CTRL)

/** Set SCT event control register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y event number [0..15]
	@param z value to write to event control register
 */
#define SCT_SetEventControl(x,y,z)     { (x)->EVENT[y].CTRL = (u32)(z); }


/* Output set register */
/** Get SCT output set register.
	@param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
	@param y output number [0..9]
	@return output set register OUT.SET
 */
#define SCT_GetOutputSet(x,y)       ((x)->OUT[y].SET)

/** Set SCT output set register.
    @param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param y output number [0..9]
    @param z value to write to output set register
 */
#define SCT_SetOutputSet(x,y,z)    { (x)->OUT[y].SET = (u32)(z); }


/* Output clear register */
/** Get SCT output clear register.
    @param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param y output number [0..9]
    @return output clear register OUT.CLR
 */
#define SCT_GetOutputClr(x,y)       ((x)->OUT[y].CLR)

/** Set SCT output clear register.
    @param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param y output number [0..9]
    @param z value to write to output clear register
*/
#define SCT_SetOutputClr(x,y,z)    { (x)->OUT[y].CLR = (u32)(z); }


/* Input multiplexer */
/** Set SCT input multiplexer register.
    @param x pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param y input multiplexer number [0..6] for SCT0/1, [0..2] for SCT2/3
    @param z value to input multiplexer register
*/
//#define SCT_SetInmux(x,y,z) { (((x) == LPC_SCT0) ? LPC_INMUX->SCT0_INMUX[y] : (((x) == LPC_SCT1) ? LPC_INMUX->SCT1_INMUX[y] : (((x) == LPC_SCT2) ? LPC_INMUX->SCT2_INMUX[y] : LPC_INMUX->SCT3_INMUX[y])))  = (u32)(z); }
#define SCT_SetInmux(x,y,z) {if ((x) == LPC_SCT0) {LPC_INMUX->SCT0_INMUX[y] = (u32)(z);} else if ((x) == LPC_SCT1) {LPC_INMUX->SCT1_INMUX[y] = (u32)(z);} else if ((x) == LPC_SCT2) {LPC_INMUX->SCT2_INMUX[y] = (u32)(z);} else {LPC_INMUX->SCT3_INMUX[y] = (u32)(z);}}

/* ----------------------------------------------------------------------------- */

extern void SCT_Stop(LPC_SCT_T *device);
extern void SCT_Start(LPC_SCT_T *device);
extern void SCT_PWM_SetupDualCapture(LPC_SCT_T *device, u8 capture_ch, u8 in_ch, u8 polarity);
extern void SCT_PWM_SetupChannel(LPC_SCT_T *device, u8 match_ch, u8 out_ch, u8 polarity);
extern void SCT_PWM_SetupPeriod(LPC_SCT_T *device, u32 freq);
extern void SCT_PWM_SetupDMABurst(LPC_SCT_T *device, u8 dc_ch, u8 dma_ch, u8 dma_0_1);
extern void SCT_PWM_StartDMABurst32(u8 dma_ch, u32 *buf, u16 size);
extern void SCT_PWM_StartDMABurst16(u8 dma_ch, u16 *buf, u16 size);

/* ----------------------------------------------------------------------------- */

 /** Get number of ticks per period
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @return	Number of ticks per period
*/
static __INLINE u32 SCT_GetTicksPerPeriod(LPC_SCT_T *device) {
 	return SCT_GetMatchReload(device, 0);
}

/** Set PWM Dutycycle in ticks.
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param	match_ch match channel used for PWM DC
    @param	dc_ticks DC in ticks
*/
static __INLINE  void SCT_SetDutyCycle(LPC_SCT_T *device, u8 match_ch, u32 ticks) {
	 SCT_SetMatchReload(device, match_ch, ticks);
}

#define sct_u32_pct15(v32, pct15) ( (((u32)(v32)>>15)*(u32)(pct15)) + ((((u32)(v32)&0x7fff)*(u32)(pct15))/0x8000) )

/** Set PWM Dutycycle in ticks.
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param	match_ch match channel used for PWM DC
    @param	dc DC in percent [0x8000 == 100%]
*/
static __INLINE  void SCT_SetDutyCyclePct(LPC_SCT_T *device, u8 match_ch, u16 dc) {
	 SCT_SetMatchReload(device, match_ch, sct_u32_pct15(SCT_GetTicksPerPeriod(device),dc));
}

 /** Get dual capture pulse length in ticks
    @param device pointer to SCT peripheral [LPC_SCT0/LPC_SCT1/LPC_SCT2/LPC_SCT3]
    @param capture_ch first capture channel [0..14] (two consecutive channels are used)
    @return	Number of ticks per pulse
*/
static __INLINE u32 SCT_GetDualCaptureTicks(LPC_SCT_T *device, u8 capture_ch) {
 	return SCT_GetCapture(device, capture_ch+1);
}
